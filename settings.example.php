<?php
// Distributed Drupal main settings.
$conf['distributed_drupal'] = array(
  'slaves' => array(
    'server1.example',
    'server2.example',
  ),
  'masters' => array(
  	'server3.example',
  ),
  'this_server_mode' => 'master',
);

// PHP's mysqlnd_ms extension related settings.
require_once './sites/all/modules/distributed_drupal/ms_callback.inc';
// Use * as a wildcard in the end of the string, if needed.
$conf['distributed_drupal_ms_callback_slave_tables'] = array(
        'sessions',
        'cache_*',
        'distributed_drupal_clear_log_slave',
);

// Session.inc replacement
//$conf['session_inc'] = 'sites/all/modules/distributed_drupal/session_7.21.inc';

// Cache backend replacement
$conf['cache_backends'][] = './sites/all/modules/distributed_drupal/mysqlcache.inc';
$conf['cache_backends'][] = './sites/all/modules/distributed_drupal/memcache.inc';
$conf['cache_default_class'] = 'MySQLDatabaseCache';