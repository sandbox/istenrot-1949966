<?php

/**
 * Replaces all the values in array with ? characters
 */
function _prepare_array_item(&$value, $key) {
  $value = '?';
}

/**
 * Defines a cache implementation for Distributed Drupal.
 */
class MySQLDatabaseCache implements DrupalCacheInterface {

  protected $dbClass;
  protected $bin;

  function __construct($bin) {
    if (db_driver() != "mysql")
      die("You did the unthinkable! Die!");
    $this->bin = $bin;
    // We use this to call default cache class methods
    $this->dbClass = new DrupalDatabaseCache($bin);
  }

  function get($cid) {
    return $this->dbClass->get($cid);
  }

  function getMultiple(&$cids) {
    return $this->dbClass->getMultiple($cids);
  }

  function clear($cid = NULL, $wildcard = FALSE) {
    // Make sure we don't trigger fatal errors while installing the module.
    if (function_exists("distributed_drupal_cc_save")) {
      distributed_drupal_cc_save($cid, $this->bin, $wildcard);    
    }

    return $this->dbClass->clear($cid, $wildcard);
  }

  function isEmpty() {
    return $this->dbClass->isEmpty();
  }

  /**
   * Implements DrupalCacheInterface::set().
   */
  function set($cid, $data, $expire = CACHE_PERMANENT) {
    $fields = array(
      'serialized' => 0,
      'created' => REQUEST_TIME,
      'expire' => $expire,
    );
    if (!is_string($data)) {
      $fields['data'] = serialize($data);
      $fields['serialized'] = 1;
    }
    else {
      $fields['data'] = $data;
      $fields['serialized'] = 0;
    }

    $fields_sql = array('cid' => $cid);
    $fields_sql += $fields;
    $fields_prepared = $fields_sql;
    array_walk($fields_prepared, '_prepare_array_item');

    $pdoConnection = Database::GetConnection('default');
    $prefix = $pdoConnection->tablePrefix($this->bin);

    try {
      $stmReplace = $pdoConnection->prepare("REPLACE INTO " . $pdoConnection->escapeTable($prefix . $this->bin) . " (" . implode(", ", array_keys($fields_sql)) . ") VALUES(" . implode(", ", array_values($fields_prepared)) . ")");
      $stmReplace->execute(array_values($fields_sql));

    }
    catch (Exception $e) {
      // The database may not be available, so we'll ignore cache_set requests.
    }
  }

}
