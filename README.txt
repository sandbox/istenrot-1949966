Overview
--------
Distributed Drupal allows a geographically distributed multi-server
installation of a single Drupal site. This is meant to improve response times
for users who would otherwise have to use a website served from the other side
of the world.

Configuration
-------------
Distributed Drupal is by no means a "plug-and-play" module. It requires a lot
of server configuration to work properly. This document will briefly document
that configuration.

DNS
---
The setup should have multiple geographically distributed web servers that
have the same domain name. This requires some kind of Geo DNS setup, so that
the same domain name gets routed to different servers based on where the
request originates. 

MySQL replication
-----------------
The system assumes that there is one MySQL server configured as the master,
and one or more MySQL servers configured as slaves.

The cache tables are not replicated due to the huge amount of data in them,
and the fact that since they are written to all the time, pretty much every
request would have to make a write query to the master server. So, all cache
tables are local. This requires us to take care of propagating cache clear
commands to all the servers, and that's what the majority of the module's code
is for.

TODO: Document replication setup.

mysqlnd_ms
----------
This is a PHP extension that is required by this module. The extension allows
the system to determine which MySQL queries should be run on the local slave
database, and which ones on the master database.

For example, all session related queries are run on the local database, as are
all caching related queries. On the master server, both master and slave
should be set up so that they point to the local database.

The user filter callback for mysqlnd_ms should be configured as 
"distributed_drupal_ms_callback".

See http://www.php.net/manual/en/book.mysqlnd-ms.php for more details.

Note that since this is done on the database level, there are no multiple
databases set up in Drupal's settings.php.

Replacement cache backend - MySQLDatabaseCache
----------------------------------------------
This is a simple class that overrides the set() method of DrupalDatabaseCache
with a MySQL optimized version. Requires the use of a MySQL database backend.

This file has an additional call to distributed_drupal_cc_save() in the
clear() method. Call is skipped if distributed_drupal module is not enabled.

TODO Document what else it does compared to the original! 

Add these lines to end of your site's settings.php:

$conf['cache_backends'][] = './sites/all/modules/distributed_drupal/mysqlcache.inc';
$conf['cache_default_class'] = 'MySQLDatabaseCache';

You need to modify the path in the example according to your specific Drupal
setup.

Modified cache backend - MemCacheDrupal
---------------------------------------
This is a modified version of MemCacheDrupal class (originally from memcache module).

This file has an additional call to distributed_drupal_cc_save() in the
clear() method. Call is skipped if distributed_drupal module is not enabled.

Add this line to end of your site's settings.php:

$conf['cache_backends'][] = './sites/all/modules/distributed_drupal/memcache.inc';



