<?php

// we have configured the callback function name as ms_callback in mysqlnd configs
// FIXME: add config option here, maybe
if(!function_exists('ms_callback')) {
  function ms_callback($connected, $query, $masters, $slaves, $last_used_connection, $in_transaction) {
    return distributed_drupal_ms_callback($connected, $query, $masters, $slaves, $last_used_connection, $in_transaction);
  }
}

/**
 * Implements the mysqlnd callback for user-defined read/write splitting and server selection.
 *
 * @param string $connected URI of the currently connected database server.
 * @param string $query Query string of the statement for which a server needs to be picked.
 * @param array $masters List of master servers to choose from.
 * @param array $slaves List of slave servers to choose from.
 * @param string $last_used_connection URI of the server of the connection used to execute the previous statement on.
 * @param boolean $in_transaction Boolean flag indicating whether the statement is part of an open transaction. If autocommit mode is turned off, this will be set to TRUE. Otherwise it is set to FALSE.
 * 
 * @return string URI of selected server.
 */
function distributed_drupal_ms_callback($connected, $query, $masters, $slaves, $last_used_connection, $in_transaction) {
  
  global $conf;
  
  if(!isset($conf['distributed_drupal_ms_callback_slave_tables']) || !is_array($conf['distributed_drupal_ms_callback_slave_tables'])) {
    // we have no settings, falling back to the plugins build-in-logic
    return NULL;
  }
  
  //preg_match_all('/INSERT ( |INTO|LOW_PRIORITY|DELAYED|HIGH_PRIORITY|IGNORE)*|UPDATE ( |LOW_PRIORITY|IGNORE)*|DELETE ( |LOW_PRIORITY|QUICK|IGNORE)*FROM|REPLACE ( |INTO|LOW_PRIORITY|DELAYED)*) (.+)( |;|$)/i', $query, $tables);

  $useSlave = FALSE;

  //$query = str_replace("\n", " ", $query); // strip linefeeds
  $query = preg_replace('/\s+/', ' ',$query); // change multiple spaces and/or linefeeds to just one space

  foreach($conf['distributed_drupal_ms_callback_slave_tables'] as $slaveTable) {
    // with wildcard, e.g. 'cache_*'
    if (substr($slaveTable,-1) != '*' && ( stristr($query, "FROM ".$slaveTable." ") || stristr($query, "INTO ".$slaveTable." ") || stristr($query, "UPDATE ".$slaveTable." ") || stristr($query, "TRUNCATE ".$slaveTable) )) {
      $useSlave = TRUE;
      break;
    }
    // with exact string, e.g. 'sessions'
    else if(substr($slaveTable,-1) == '*' && ( stristr($query, "FROM ".substr($slaveTable,0,-1)) || stristr($query, "INTO ".substr($slaveTable,0,-1)) || stristr($query, "UPDATE ".substr($slaveTable,0,-1)) || stristr($query, "TRUNCATE ".substr($slaveTable,0,-1)) )) {
      $useSlave = TRUE;
      break;
    }
  }

  /*
  static $slave_idx = 0;
  static $num_slaves = NULL;
  if (is_null($num_slaves)) {
    $num_slaves = count($slaves);
  }

  static $master_idx = 0;
  static $num_masters = NULL;
  if (is_null($num_masters)) {
    $num_masters = count($masters);
  }
  */

  $where = mysqlnd_ms_query_is_select($query);
  switch ($where) {
    case MYSQLND_MS_QUERY_USE_MASTER:    
      // check if we should use slave
      if($useSlave) {
        //$ret = $slaves[$slave_idx++ % $num_slaves];
        $ret = $slaves[0];
      }
      else {
        //$ret = $masters[$master_idx++ % $num_masters];
        $ret = $masters[0];
      }
      break;
    case MYSQLND_MS_QUERY_USE_SLAVE:
      /* SELECT or SQL hint for using slave */
      //$ret = $slaves[$slave_idx++ % $num_slaves];
      $ret = $slaves[0];
      break;
    case MYSQLND_MS_QUERY_LAST_USED:
      $ret = $last_used_connection;
      break;
    default:
      /* default: fallback to the plugins build-in logic */
      $ret = NULL;
      break;

  }

  return $ret;
}